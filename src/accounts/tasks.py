import datetime

from celery import shared_task
from django.core.mail import send_mail
from django.db.models.signals import pre_save
from django.dispatch import receiver

from accounts.models import User


# @receiver(pre_save, sender=User)
# def check_new_user(sender, **kwargs):
#     send_mail(
#         'New user created',
#         f'New user {sender} created at {datetime.datetime.now()}',
#         'from quiz@example.com',
#         ['antonzakharov88@gmail.com'],
#         fail_silently=False,
#     )


# @shared_task
# def send_email():
#     print('Email sended')
#     return send_mail(
#         'New user created',
#         f'New user  created at {datetime.datetime.now()}',
#         'from quiz@example.com',
#         ['antonzakharov88@gmail.com'],
#         fail_silently=False,
#     )

