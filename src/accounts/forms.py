from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.forms import Form, fields

from django import forms

from accounts.models import User


class AccountRegistrationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = User
        fields = ['username', 'first_name', 'last_name', 'email']


class AccountUpdateForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'image']


class ContactUs(Form):
    subject = fields.CharField(max_length=256, empty_value='message from testify')
    message = fields.CharField(widget=forms.Textarea)
