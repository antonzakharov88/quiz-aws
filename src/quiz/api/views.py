from rest_framework import generics

from quiz.api.serializers import TestSerializer
from quiz.models import Test


class TestListCreateView(generics.ListCreateAPIView):
    queryset = Test.objects.all()
    serializer_class = TestSerializer


# class TestUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
class TestUpdateDeleteView(generics.RetrieveUpdateAPIView):

    queryset = Test.objects.all()
    serializer_class = TestSerializer
