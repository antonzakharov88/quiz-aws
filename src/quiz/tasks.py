import datetime
import time
from celery import shared_task
from django.core.mail import send_mail
from django.db.models.signals import post_save
from django.dispatch import receiver

from accounts.models import User
from quiz.models import Result


@shared_task
def mine_bitcoin(n):
    time.sleep(n)


@shared_task
def cleanup_outdated_results():
    outdated_tests = Result.objects.filter(
        state=Result.STATE.NEW,
        write_date__lte=datetime.datetime.now() - datetime.timedelta(seconds=7 * 24 * 3600)
    )
    outdated_tests.delete()

    print('Outdated results deleted!')


@receiver(post_save, sender=User)
def check_new_user(sender, instance, created, **kwargs):
    if created:
        user_id = instance.id
        email = instance.email
        check_new_user_results.delay(3600 * 24, user_id, email)


@shared_task
def check_new_user_results(n, user_id, email):
    time.sleep(n)
    results = Result.objects.filter(
        user=user_id
    )
    if results:
        pass
    else:
        send_mail(
            'HELLO!',
            f'Welcome to the Quiz platform',
            'quiz@example.com',
            [f'{email}'],
            fail_silently=False,
        )


@shared_task
def check_uncompleted_tests():
    uncompleted_tests = Result.objects.filter(
        state=Result.STATE.NEW,
        write_date__gte=datetime.datetime.now() - datetime.timedelta(seconds=5 * 24 * 3600)
    )
    if uncompleted_tests:

        for user_id in uncompleted_tests.values_list('user_id', flat=True):
            email_list = User.objects.filter(id=user_id).values_list('email', flat=True)

            for email in email_list:
                send_mail(
                    'HELLO!',
                    f'You have uncompleated test, please finish it',
                    'quiz@example.com',
                    [f'{email}'],
                    fail_silently=False,
                )
