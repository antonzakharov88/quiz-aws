from django.db import models
from django.core.validators import MaxValueValidator
from accounts.models import User

# Create your models here.
from core.models import BaseModel
from core.utils import generate_uuid


class Test(BaseModel):
    QUESTION_MIN_LIMIT = 3
    QUESTION_MAX_LIMIT = 20

    class LEVEL_CHOICES(models.IntegerChoices):
        BASIC = 0, "Basic"
        MIDDLE = 1, "Middle"
        ADVANCED = 2, "Advanced"

    # topic = models.ForeignKey(to=Topic, related_name='tests', null=True, on_delete=models.SET_NULL)
    uuid = models.UUIDField(default=generate_uuid, db_index=True, unique=True)
    title = models.CharField(max_length=64)
    description = models.TextField(max_length=1024, null=True, blank=True)
    level = models.PositiveSmallIntegerField(choices=LEVEL_CHOICES.choices, default=LEVEL_CHOICES.MIDDLE)
    image = models.ImageField(default='default.png', upload_to='covers')

    def questions_count(self):
        return self.questions.count()

    def _get_best_result(self):
        if not self.results.all():
            return {'score': 'N/A', 'user': 'N/A'}
        _results_list = []
        for result in self.results.all():
            _results_list.append(result.num_correct_answers)
        best_result = max(_results_list)
        best_users = []
        time_spend = []
        for result in self.results.all():
            if result.num_correct_answers == best_result:
                best_users.append(result.user.last_name)
                time_spend.append(result.time_spent())
        if len(best_users) > 1:
            leader_dict = dict(zip(time_spend, best_users))
            min_time = min(time_spend)
            best_users = leader_dict[min_time]

        return {'score': best_result, 'user': best_users}

    def get_best_result_score(self):
        return self._get_best_result()['score']

    def get_best_user(self):
        return self._get_best_result()['user']

    def get_time_last_run(self):
        return self.results.last().create_date

    def has_unfinished_run(self):
        return self.results.exclude(
            state=Result.STATE.FINISHED
        ).exists()

    def __str__(self):
        return f'{self.title}'


class Question(models.Model):
    ANSWER_MIN_LIMIT = 3
    ANSWER_MAX_LIMIT = 6

    test = models.ForeignKey(to=Test, related_name='questions', on_delete=models.CASCADE)
    order_number = models.PositiveSmallIntegerField(
        validators=[MaxValueValidator(Test.QUESTION_MAX_LIMIT)])
    text = models.CharField(max_length=64)

    def __str__(self):
        return f'{self.text}'


class Choice(models.Model):
    CHOICE_MIN_LIMIT = 1
    CHOICE_MAX_LIMIT = 3

    text = models.CharField(max_length=64)
    question = models.ForeignKey(to=Question, related_name='choices', on_delete=models.CASCADE)
    is_correct = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.text}'


class Result(BaseModel):
    class STATE(models.IntegerChoices):
        NEW = 0, "New"
        FINISHED = 1, "Finished"

    user = models.ForeignKey(to=User, related_name='results', on_delete=models.CASCADE)
    test = models.ForeignKey(to=Test, related_name='results', on_delete=models.CASCADE)
    state = models.PositiveSmallIntegerField(default=STATE.NEW, choices=STATE.choices)
    uuid = models.UUIDField(default=generate_uuid, db_index=True, unique=True)
    current_order_number = models.PositiveSmallIntegerField(null=True)

    num_correct_answers = models.PositiveSmallIntegerField(
        default=0,
        validators=[
            MaxValueValidator(Test.QUESTION_MAX_LIMIT)
        ]
    )
    num_incorrect_answers = models.PositiveSmallIntegerField(
        default=0,
        validators=[
            MaxValueValidator(Test.QUESTION_MAX_LIMIT)
        ]
    )

    def update_result(self, order_number, question, selected_choices):
        correct_choices = [
            choice.is_correct
            for choice in question.choices.all()
        ]

        correct_answer = True
        for x, y in zip(correct_choices, selected_choices):
            correct_answer &= (x == y)

        self.num_correct_answers += int(correct_answer)
        self.num_incorrect_answers += 1 - int(correct_answer)
        self.current_order_number = order_number
        if order_number == question.test.questions_count():
            self.state = self.STATE.FINISHED
        self.save()

    def points(self):
        return max(0, self.num_correct_answers - self.num_incorrect_answers)

    def success_rate(self):
        result = round(self.num_correct_answers / self.test.questions_count() * 100, 2)
        return f' {result} %'

    def time_spent(self):
        time_spent = self.write_date - self.create_date
        return time_spent

    def save(self, *args, **kwargs):
        if self.state == self.STATE.FINISHED:
            self.user.rating += self.points()
            self.user.save()
        super().save(*args, **kwargs)
