asgiref==3.3.1
backcall==0.2.0
decorator==4.4.2
Django==3.1.6
django-crispy-forms==1.11.0
django-debug-toolbar==3.2
django-extensions==3.1.0
django-filter==2.4.0
django-restframework==0.0.1
djangorestframework==3.12.2
ftps==0.1.0
ipython==7.20.0
ipython-genutils==0.2.0
jedi==0.18.0
Markdown==3.3.3
parso==0.8.1
pexpect==4.8.0
pickleshare==0.7.5
Pillow==8.1.0
prompt-toolkit==3.0.14
psycopg2==2.8.6
ptyprocess==0.7.0
pycurl==7.43.0.6
Pygments==2.7.4
pytz==2021.1
six==1.15.0
sqlparse==0.4.1
traitlets==5.0.5
wcwidth==0.2.5
